<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCominsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('com_title_rus');
            $table->text('com_desc_rus');
            $table->string('com_title_kaz')->nullable();
            $table->text('com_desc_kaz')->nullable();
            $table->string('com_title_eng')->nullable();
            $table->text('com_desc_eng')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comins');
    }
}
