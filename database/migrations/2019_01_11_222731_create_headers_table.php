<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('headers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_rus');
            $table->string('title_kaz')->nullable();
            $table->string('title_eng')->nullable();
            $table->string('main_rus');
            $table->string('main_kaz')->nullable();
            $table->string('main_eng')->nullable();
            $table->string('logo');
            $table->text('main_cont_rus');
            $table->text('main_cont_kaz')->nullable();
            $table->text('main_cont_eng')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('headers');
    }
}
