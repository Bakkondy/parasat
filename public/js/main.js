$(document).ready(function(){
	
	$('.gallery-slider').owlCarousel({
		center:true,
	    loop:true,
	    margin:-75,
	    stagePadding: 100,
    	//animateIn: 'fadeInRight',
    	//animateOut: 'fadeOut',
	    mouseDrag:true,
	    touchDrag:true,
	    autoplay:true,
        autoplayTimeout:3000,
        smartSpeed:500, 
        autoplayHoverPause: true,
        nav: false,
        dots: false,
	    responsive:{
	        0:{
	            items:1,
	            margin: 0,
	            stagePadding: 0,
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    },
	});

    $('a[href^="#"]').click(function(e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 1000);
    });

	$('.news-slider').owlCarousel({
		center:true,
	    loop:true,
	    margin:0,
	    stagePadding: 0,
    	//animateIn: 'fadeInRight',
    	//animateOut: 'fadeOut',
	    mouseDrag:true,
	    touchDrag:true,
	    autoplay:true,
        autoplayTimeout:3000,
        smartSpeed:500, 
        autoplayHoverPause: true,
        nav: false,
        dots: false,
	    responsive:{
	        0:{
	            items:1,
	            margin: 0,
	            stagePadding: 0,
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    },
	});

	if($(window).width() < 1024){

		$('.mobile-burger').click(function() {
			$('.header-popup').fadeToggle();
		});
		$('.popup-close').click(function() {
			$('.header-popup').fadeToggle();
		});

		$('.desk-text').insertAfter('.pci');
		$('.head-li').insertAfter('.desk-text');
		$('.desk-contacts').insertAfter('.head-li');
		$('.language').insertAfter('.desk-contacts');

	}

});