<?php


Route::get('/', 'RusController@index')->name('index_rus');
Route::get('/kz', 'KazController@index')->name('index_kaz');
Route::get('/en', 'EngController@index')->name('index_eng');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
