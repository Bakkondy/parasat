<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <title>{{ $header->title_kaz }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="{{ asset(Voyager::image($header->logo)) }}" />

    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="libs/animate/animate.css">
    <link rel="stylesheet" href="libs/fancybox/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="libs/fullpage/css/fullpage.min.css">
    <link rel="stylesheet" href="libs/mScroll-bar/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="libs/owl-carousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="libs/owl-carousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="libs/owl-carousel/css/owl.theme.green.min.css">
    <link rel="stylesheet" href="libs/remodal/css/remodal.css">
    <link rel="stylesheet" href="libs/remodal/css/remodal-default-theme.css">
    <link rel="stylesheet" href="libs/select2/css/select2.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/responsive.css">

    <script src="libs/jquery/jquery-3.3.1.min.js"></script>
    <script src="libs/fancybox/js/jquery.fancybox.min.js"></script>
    <script src="libs/fullpage/js/fullpage.min.js"></script>
    <script src="libs/maskedinput/maskedinput.min.js"></script>
    <script src="libs/mScroll-bar/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="libs/owl-carousel/js/owl.carousel.min.js"></script>
    <script src="libs/owl-carousel/js/owl.carousel2.thumbs.js"></script>
    <script src="libs/parallax/jquery.localscroll-1.2.7-min.js"></script>
    <script src="libs/parallax/jquery.parallax-1.1.3.js"></script>
    <script src="libs/pinto/imagesloaded.pkgd.min.js"></script>
    <script src="libs/pinto/jquery.pinto.js"></script>
    <script src="libs/remodal/js/remodal.min.js"></script>
    <script src="libs/select2/js/select2.full.min.js"></script>
    <script src="libs/stickyeah/stickyeah.js"></script>
    <script src="libs/stickyfill-master/dist/stickyfill.min.js"></script>
    <script src="libs/wow/wow.min.js"></script>
    <script src="js/main.js"></script>

</head>

<body>

<header>
    <div class="lang">
        <div class="wrapper">
            <div class="row">
                <ul class="language">
                    <li><a href="{{route('index_kaz')}}" class="active">Қаз</a></li>
                    <li><a href="{{route('index_rus')}}">Рус</a></li>
                    <li><a href="{{route('index_eng')}}">Eng</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="desktop-header-wrap">
        <div class="wrapper">
            <div class="row">

                <div class="desktop-header">
                    <div class="desk-logo">
                        <a href="{{route('index_kaz')}}">
                            <img src="{{ asset(Voyager::image($header->logo)) }}" alt="">
                        </a>
                    </div>
                    <div class="desk-text">
                        <p>{!! $header->main_kaz !!}</p>
                        <p>{!! $header->main_rus !!}</p>
                        <p>{!! $header->main_eng !!}</p>
                    </div>
                    <div class="desk-contacts">
                        <p>{!! $header->main_cont_kaz !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu-list">
        <div class="wrapper">
            <div class="row">
                <ul class="head-li">
                    @foreach($menulist as $item)
                        <li>
                            <a href="#{{ $item->menu_href }}">{{ $item->menu_kaz }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="mobile-header">
        <div class="wrapper">
            <div class="row">
                <div class="mobile-wrap">
                    <div class="mobile-logo">
                        <a href="{{route('index_kaz')}}">
                            {{ $header->title_kaz }}
                        </a>
                    </div>
                    <div class="mobile-burger">
                        <i class="fas fa-bars"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-popup">
        <div class="wrapper">
            <div class="row">
                <div class="popup-content">
                    <div class="pci"></div>
                </div>
            </div>
        </div>
        <div class="popup-close">
            <i class="fas fa-times"></i>
        </div>
    </div>
</header>

<!-- CONTENT PART  -->
<section id="news">
    <div class="wrapper">
        <div class="row">
            <div class="content">
                <div class="news">
                    <div class="news-left">
                        <div class="owl-carousel owl-theme news-slider">
                            @foreach($news as $item)
                                <div class="news-every">
                                    <div class="ne-40">
                                        <h2>{{ $item->news_title_kaz }}</h2>
                                        <p>{!! $item->news_desc_kaz !!}</p>
                                    </div>
                                    <div class="ne-60" style="background: url({{ asset(Voyager::image($item->image)) }});">

                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="news-right">
                        <div class="nr-bg" style="background: url({{ asset(Voyager::image($president->image)) }});">
                            <div class="nr-text">
                                <h4>{{ $president->pre_title_kaz }}</h4>
                                <p>{!! $president->pre_desc_kaz !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="about">
    <div class="wrapper">
        <div class="row">
            <div class="heading">
                <h2>Біз туралы</h2>
                <img src="img/oyu.gif" alt="">
            </div>
            <div class="content">
                <div class="about-text">
                    {!! $about->about_kaz !!}
                </div>
            </div>
        </div>
    </div>
</section>
<section id="gallery">
    <div class="wrapper">
        <div class="row">
            <div class="heading">
                <h2>ГАЛЕРЕЯ</h2>
                <img src="img/oyu.gif" alt="">
            </div>
            <div class="content">
                <div class="gallery-slider owl-carousel owl-theme">
                    @foreach($gallery as $item)
                        <div class="item" style="background: url({{ asset(Voyager::image($item->image)) }});">

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<section id="npa">
    <div class="wrapper">
        <div class="row">
            <div class="heading">
                <h2>Нормативті-құқықтық актілер</h2>
                <img src="img/oyu.gif" alt="">
            </div>
            <div class="content">
                <div class="acts">
                    <ul>
                        @foreach($act as $item)
                            <li>
                                <a href="{{ $item->acts_doc }}">{{ $item->acts_kaz }} </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="join">
    <div class="wrapper">
        <div class="row">
            <div class="heading">
                <h2>Біздің ұйымға кіру</h2>
                <img src="img/oyu.gif" alt="">
            </div>
            <div class="content">
                <div class="became">
                    @foreach($comin as $item)
                        <div class="every-became">
                            <h3>{{ $item->com_title_kaz }}</h3>
                            <p>{!! $item->com_desc_kaz !!}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<section id="projects">
    <div class="wrapper">
        <div class="row">
            <div class="heading">
                <h2>Жобалар</h2>
                <img src="img/oyu.gif" alt="">
            </div>
            <div class="content">
                <div class="projects">
                    @foreach($project as $item)
                        <div class="project-every">
                            <div class="project-img">
                                <img src="{{ asset(Voyager::image($item->image)) }}" alt="">
                            </div>
                            <div class="project-content">
                                <h3>{{ $item->pro_title_kaz }}</h3>
                                <p>{!! $item->pro_desc_kaz !!}</p>
                                <a href="{{ $item->href }}" target="_blank">Толығырақ</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<section id="contacts">
    <div class="wrapper">
        <div class="row">
            <div class="heading">
                <h2>Байланыс</h2>
                <img src="img/oyu.gif" alt="">
            </div>
            <div class="content">
                <div class="contacts">
                    <div class="contacts-address">
                        <p>{!! $contact->cont_kaz !!}</p>
                    </div>
                </div>
                <div class="socials" style="display: block !important;">
                    <ul>
                        @foreach($social as $item)
                            <li><a href="{{ $item->href }}"><img src="{{ asset(Voyager::image($item->image)) }}" alt="{{ $item->name }}"></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- CONTENT PART END  -->

<footer>
    <div class="wrapper">
        <div class="row">
            <div class="footer">
                <div class="foot1">
                    <p>Барлық құқық қорғалған </p>
                </div>
                <div class="foot2">
                    <p>2018 </p>
                </div>
                <div class="foot3">
                    <a href="">@qbiggroup </a>
                </div>
            </div>
        </div>
    </div>
</footer>

<!--REMODAL POPUP PART-->
<div class="remodal-bg">
    <div id="qunit"></div>
</div>

<!--a href="" data-remodal-target="modal">Modal</a-->
<div class="remodal" data-remodal-id="modal" data-remodal-options="hashTracking: false">
    <div class="remodal-content">
        <div class="row">
            Content here...
        </div>
    </div>
    <a data-remodal-action="close" class="remodal-close"></a>
</div>
<!--REMODAL POPUP PART END-->

</body>
</html>