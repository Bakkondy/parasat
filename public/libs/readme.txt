1. WOW.js
WOW animation + animate.css
https://wowjs.uk/docs.html

data-wow-duration: Change the animation duration
data-wow-delay: Delay before the animation starts
data-wow-offset: Distance to start the animation (related to the browser bottom)
data-wow-iteration: Number of times the animation is repeated

<section class="wow slideInRight" data-wow-offset="10"  data-wow-iteration="10"></section>
wow = new WOW(
  {
  boxClass:     'wow',      // default
  animateClass: 'animated', // default
  offset:       0,          // default
  mobile:       true,       // default
  live:         true        // default
}
)
wow.init();

new WOW().init();

2. Fancybox.js
Fancybox
https://www.fancyapps.com/fancybox/3/docs/

<a href="image_1.jpg" data-fancybox="gallery" data-caption="Caption #1">
	<img src="thumbnail_1.jpg" alt="" />
</a>


3. Fullpage.js
https://github.com/alvarotrigo/fullPage.js/#fullpagejs
<div id="fullpage">
	<div class="section active">Some section</div>
	<div class="section">Some section</div>
	<div class="section">Some section</div>
	<div class="section">Some section</div>
</div>

$(document).ready(function() {
	$('#fullpage').fullpage({
		//options here
		autoScrolling:true,
		scrollHorizontally: true
	});

	//methods
	$.fn.fullpage.setAllowScrolling(false);
});

4. Maskedinput.js
https://github.com/digitalBush/jquery.maskedinput

jQuery(function($){
   $("#date").mask("99/99/9999");
   $("#phone").mask("(999) 999-9999");
   $("#tin").mask("99-9999999");
   $("#ssn").mask("999-99-9999");
});

5. Select2.js
https://select2.org/

$('.js-example-basic-single').select2();
$('.js-example-basic-multiple').select2();

6. Owlcarousel.js

$('.owl-slider-main').owlCarousel({
    center:false,
    loop:false,
    margin:0,
    nav:false,
    dots:false,
    mouseDrag:true,
    touchDrag:true,
    autoplay:true,
    autoplayTimeout:4000,
    smartSpeed:1000, 
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

