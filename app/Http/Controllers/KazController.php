<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Header;
use App\Menulist;
use App\News;
use App\President;
use App\About;
use App\Gallery;
use App\Act;
use App\Comin;
use App\Project;
use App\Contact;
use App\Social;

class KazController extends Controller
{
    public function index()
    {
        $header = Header::first();
        $menulist = Menulist::all();
        $news = News::all();
        $president = President::first();
        $about = About::first();
        $gallery = Gallery::all();
        $act = Act::all();
        $comin = Comin::all();
        $project = Project::all();
        $contact = Contact::first();
        $social = Social::all();

        return view('parasat_kaz', compact(
            'header',
            'menulist',
            'news',
            'president',
            'about',
            'gallery',
            'act',
            'comin',
            'project',
            'contact',
            'social'
        ));
    }
}
