<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('href');
            $table->string('pro_title_rus');
            $table->string('pro_desc_rus');
            $table->string('pro_title_kaz')->nullable();
            $table->string('pro_desc_kaz')->nullable();
            $table->string('pro_title_eng')->nullable();
            $table->string('pro_desc_eng')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
