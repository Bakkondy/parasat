<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presidents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('pre_title_rus');
            $table->text('pre_desc_rus');
            $table->string('pre_title_kaz')->nullable();
            $table->text('pre_desc_kaz')->nullable();
            $table->string('pre_title_eng')->nullable();
            $table->text('pre_desc_eng')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presidents');
    }
}
